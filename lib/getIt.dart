import 'package:get_it/get_it.dart';
import 'package:pipe_count/ui/controllers/action_provider.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerSingleton<ActionProvider>(ActionProvider());
  // getIt.registerFactory<ActionProvider>(() => ActionProvider());

}
