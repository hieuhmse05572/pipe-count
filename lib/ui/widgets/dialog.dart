import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pipe_count/core/helper/typeHelper.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/models/type.dart';
import 'package:pipe_count/core/utils/constants.dart';
import 'package:pipe_count/ui/views/_view.dart';
import 'package:pipe_count/ui/views/home_view.dart';
import 'package:pipe_count/ui/views/type_camera.dart';



dialogChooseImage(BuildContext context, Group group, Project project) {
  // Future getImageFromGallery() async {
  //   return await ImagePicker.platform.pickImage(source: ImageSource.gallery);
  // }
  Future getImageFromGallery() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 80);
    if (image != null && image.path != null) {
      File rotatedImage =
          await FlutterExifRotation.rotateImage(path: image.path);
      return rotatedImage;
    }
  }

  // Future getImageFromCamera() async {
  //   return await ImagePicker.platform.pickImage(source: ImageSource.camera);
  // }
  Future getImageFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 80);
    if (image != null && image.path != null) {
      File rotatedImage =
          await FlutterExifRotation.rotateImage(path: image.path);
      return rotatedImage;
    }
  }

  return Dialog(
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 500,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(""),
              Text(
                'Photo',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              InkWell(
                child: Icon(Icons.cancel),
                onTap: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
          Divider(),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: 40,
                child: RaisedButton(
                  color: Colors.white,
                  elevation: 0,
                  onPressed: () async {
                    // Navigator.pop(context);
                    File image = await getImageFromGallery();
                    if(image != null)
                      Navigator.of(context).pushReplacement(PageRouteBuilder(
                        pageBuilder: (context, animation, anotherAnimation) {
                          return SafeArea(
                            child: Game(imagePathFromLocal: image.path, group: group, project: project, edit: false
                            ),
                          );
                        },
                        transitionDuration: Duration(milliseconds: 0),
                        transitionsBuilder:
                            (context, animation, anotherAnimation, child) {
                          animation = CurvedAnimation(
                              curve: Curves.fastOutSlowIn, parent: animation);
                          return Align(
                              child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ));
                        }));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.image),
                      SizedBox(
                        width: 10,
                        height: 1,
                      ),
                      Text(
                        'From Gallery',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Divider(),
              Container(
                height: 40,
                child: RaisedButton(
                  color: Colors.white,
                  elevation: 0,
                  splashColor: Colors.grey,
                  onPressed: () async {
                    // Navigator.pop(context);
                    File image = await getImageFromCamera();
                    if(image != null)
                    Navigator.of(context).pushReplacement(PageRouteBuilder(
                        pageBuilder: (context, animation, anotherAnimation) {
                          return SafeArea(
                            child: Game(imagePathFromLocal: image.path, group: group, project: project, edit: false),
                          );
                        },
                        transitionDuration: Duration(milliseconds: 0),
                        transitionsBuilder:
                            (context, animation, anotherAnimation, child) {
                          animation = CurvedAnimation(
                              curve: Curves.fastOutSlowIn, parent: animation);
                          return Align(
                              child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ));
                        }));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.camera_alt),
                      SizedBox(
                        width: 10,
                        height: 1,
                      ),
                      Text(
                        'Take Photo',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    ),
  );
}

dialogConfirmRemove(BuildContext context, String title, String content) {
  return Dialog(
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                // 'Delete this Project',
                title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  content,
                  // 'Do you want to delete this project?',
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, "-1");
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, "1");
                  },
                  child: Text(
                    'Ok',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    ),
  );
}

class showListType extends StatefulWidget {
  @override
  _showListTypeState createState() => _showListTypeState();
}
class _showListTypeState extends State<showListType> {
  List<Widget> titles = List();
  final typeHelper =  TypeHelper();
  TextEditingController textEditingController = new TextEditingController();
  Widget listTitle(TypeSteel type) {
    return InkWell(
      onTap: () {
        Navigator.pop(context, type);
      },
      child: Container(
        margin: EdgeInsets.only(top: 15, left: 10, right: 10, bottom: 10),
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(
            color: Colors.blueGrey.withOpacity(0.3),
            width: 0.5,
          ),
        )),
        child: Text("${type.name}"),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    typeHelper.getTypes("").then((value) {
      setState(() {
        titles  = new List();
        value.forEach((element) {
          titles.add(listTitle(element));
        });
      });
    });

  }

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 65,
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            controller: textEditingController,
            onChanged: (value) {
            typeHelper.getTypes(value).then((value) {
              setState(() {
                titles  = new List();
                value.forEach((element) {
                  titles.add(listTitle(element));
                });
              });
            });
            },
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
                hintText: "Enter type to search",
                // labelText: "Search",
                fillColor: Colors.white,
                // filled: true,
                focusColor: Colors.white,
                suffixIcon: Icon(
                  Icons.search,
                  color: Colors.black,
                ),
                border: OutlineInputBorder()),
          ),
        ),
        Container(
          height: 400,
          child: ListView(children: titles),
        ),
      ],
    );
  }
}

dialogListType(BuildContext context) {
  return Dialog(
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Choose Type',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ],
          ),
          showListType(),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, null);
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () async {
                    // Navigator.pop(context);
                    String input = await DialogUtil.showNewTypeDialog(context);
                    if(input != "-1"){
                      TypeSteel type = new  TypeSteel(0, input);
                      TypeHelper().createType(type).then((value){
                      Navigator.pop(context, type);
                      });
                    }
                  },
                  child: Text(
                    'Add',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    ),
  );
}

dialogMessage(BuildContext context, String title, String msg) {
  return Dialog(
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                // 'Counting...',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              InkWell(
                child: Icon(
                  Icons.clear,
                  color: Colors.purple,
                ),
                onTap: () {
                  Navigator.pop(context, "0");
                },
              )
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                // "Please wait !!!",
                msg,
                style: TextStyle(fontSize: 15),
              ),
            ],
          ),
          SizedBox(
            height: 10,
            width: 1,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 7),
            child: LinearProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.pink),
              backgroundColor: Colors.pink.withOpacity(0.4),
              // valueColor: Colors.pink,
            ),
          )
        ],
      ),
    ),
  );
}

dialogNewType(BuildContext context) {
  TextEditingController textEditingController = TextEditingController();
  return Dialog(
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'New Type',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Enter a name for this Type',
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
                ),
              ],
            ),
          ),
          TextField(
            controller: textEditingController,
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, "-1");
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () async {
                    Navigator.pop(context, "${textEditingController.text}");
                  },
                  child: Text(
                    'Save',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    ),
  );
}

dialogCheckNote(BuildContext context) {
  TextEditingController textEditingController = new TextEditingController();
  return Dialog(
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'New Project',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Enter a name for this project',
                  style: TextStyle(fontWeight: FontWeight.normal, fontSize: 14),
                ),
              ],
            ),
          ),
          TextField(
            controller: textEditingController,
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () {
                    Navigator.pop(context, "-1");
                  },
                  child: Text(
                    'Cancel',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all<double>(0),
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.indigo),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                  onPressed: () async {
                    Navigator.pop(context, textEditingController.text);
                  },
                  child: Text(
                    'Save',
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    ),
  );
}

class DialogUtil {
  static Future<String> showAddDialog(BuildContext context) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogCheckNote(context);
      },
    );
    return text;
  }

  static Future<String> showConfirmDialog(BuildContext context, String title, String content) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogConfirmRemove(context, title, content );
      },
    );
    return text;
  }

  static Future<TypeSteel> showListTypeDialog(BuildContext context) async {
    TypeSteel type = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogListType(context);
      },
    );
    return type;
  }

  static Future<String> showNewTypeDialog(BuildContext context) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogNewType(context);
      },
    );
    return text;
  }

  static Future<String> showChooseImage(
      BuildContext context, Group group, Project project) async {
    String text = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogChooseImage(context, group, project);
      },
    );
    return text;
  }
}
