import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final int index;
  final IconData icon;
  final int active;
  final Function onPress;

  ActionButton({this.index, this.icon, @required this.onPress, this.active});

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = active == index ? Colors.blue : Colors.white;

    return Expanded(
      child: Container(
        margin: EdgeInsets.all(2),
        child: ElevatedButton(
          onPressed: onPress,
          child: Icon(
            icon,
            color: Colors.black,
            size: 28,
          ),
          style: ButtonStyle(
              // shape: MaterialStateProperty.all<OutlinedBorder>(),
              // overlayColor:
              // MaterialStateProperty.all<Color>(Colors.blue),
              backgroundColor:
                  MaterialStateProperty.all<Color>(backgroundColor)),
        ),
      ),
    );
  }
}

class Button extends StatelessWidget {
  final Icon icon;
  final Function onPress;
  final String title;
  Button({this.icon, @required this.onPress, this.title});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.all(2),
        child: ElevatedButton(
          onPressed: () {
            onPress();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              icon != null ? icon : SizedBox(),
              Text(
                title,
                style: TextStyle(color: Colors.black, fontSize: 13),
              ),
            ],
          ),
          style: ButtonStyle(
              overlayColor: MaterialStateProperty.all<Color>(Colors.blue),
              backgroundColor: MaterialStateProperty.all<Color>(Colors.white)),
        ),
      ),
    );
  }
}

class ButtonGradian extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return s;
  }

  Widget s = GestureDetector(
    onTap: () {},
    child: Container(
      width: 100,
      height: 50,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.blueGrey,
            Colors.white,
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            offset: Offset(5, 5),
            blurRadius: 10,
          )
        ],
      ),
      child: Center(
        child: Text(
          'Press',
          style: TextStyle(
            color: Colors.white,
            fontSize: 30,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    ),
  );

  Widget gradientButton = Container(
    child: RaisedButton(
      onPressed: () {},
      textColor: Colors.white,
      padding: const EdgeInsets.all(0.0),
      child: Container(
        // width: 300,
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
          colors: [
            Color.fromARGB(255, 148, 231, 225),
            Color.fromARGB(255, 62, 182, 226)
          ],
        )),
        padding: const EdgeInsets.all(10.0),
        child: Text(
          "Gradient Button",
          textAlign: TextAlign.center,
        ),
      ),
    ),
  );
}
