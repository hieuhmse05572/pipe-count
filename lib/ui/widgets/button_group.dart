import 'package:flutter/material.dart';
import 'package:pipe_count/core/enum/mode.dart';

import 'button.dart';

class BottomLeftGroupButton extends StatelessWidget {
  BottomLeftGroupButton(
      {@required this.onBackPress, @required this.onHidePress});

  final Function onBackPress;
  final Function onHidePress;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        height: 70,
        width: 70,
        margin: EdgeInsets.all(5),
        child: Column(
          children: [
            // Expanded(
            //   child: Container(
            //     margin: EdgeInsets.all(2),
            //     child: ElevatedButton(
            //       onPressed: onDrawPress,
            //       child: Icon(
            //         Icons.panorama_photosphere,
            //         color: Colors.black,
            //         size: 31,
            //       ),
            //       style: ButtonStyle(
            //           overlayColor:
            //               MaterialStateProperty.all<Color>(Colors.blue),
            //           backgroundColor:
            //               MaterialStateProperty.all<Color>(Colors.white)),
            //     ),
            //   ),
            // ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(2),
                child: ElevatedButton(
                  onPressed: onHidePress,
                  child: Icon(
                    Icons.lock,
                    color: Colors.black,
                    size: 31,
                  ),
                  style: ButtonStyle(
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                ),
              ),
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(2),
                child: ElevatedButton(
                  onPressed: onBackPress,
                  child: Icon(
                    Icons.keyboard_return,
                    color: Colors.black,
                    size: 31,
                  ),
                  style: ButtonStyle(
                      overlayColor:
                          MaterialStateProperty.all<Color>(Colors.blue),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.white)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TopLeftGroupButton extends StatelessWidget {
  final Function onCountPress;
  final Function onSavePress;
  TopLeftGroupButton({@required this.onCountPress, @required this.onSavePress});
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topRight,
      child: Container(
        margin: EdgeInsets.all(5),
        height: 40,
        width: onSavePress == null ? 105 : 210,
        // color: Colors.white.withOpacity(0.3),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            onCountPress != null
                ? Button(
                    onPress: onCountPress,
                    title: "COUNT",
                    icon: Icon(
                      Icons.check_box,
                      color: Colors.green,
                    ),
                  )
                : Container(),
            onSavePress != null
                ? Button(
                    onPress: onSavePress,
                    title: "Save",
                    icon: Icon(
                      Icons.save,
                      color: Colors.red,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}

class ResultButton extends StatelessWidget {
  ResultButton({this.result});
  final int result;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.8),
          borderRadius: BorderRadius.circular(8),
        ),
        height: 40,
        width: 90,
        child: Center(
            child: Text(
          result.toString(),
          style: TextStyle(color: Colors.black, fontSize: 23),
        )),
      ),
    );
  }
}

class ActionButtons extends StatefulWidget {
  ActionButtons(
      {@required this.onDrawPress,
      @required this.onAddPress,
      @required this.onRemovePress,
      @required this.onChangeMode,
      @required this.mode,
      this.onMove,
      @required this.active});
  final int active;
  final Function onDrawPress;
  final Function onAddPress;
  final Function onRemovePress;
  final Function onChangeMode;
  final Function onMove;

  final MODE mode;
  @override
  _ActionButtonsState createState() => _ActionButtonsState();
}

class _ActionButtonsState extends State<ActionButtons> {
  IconData icon1, icon2, icon3;
  String modeStr = "";
  double height;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.mode == MODE.draw) {
      icon1 = Icons.swipe;
      icon2 = Icons.data_usage;
      icon3 = Icons.clear;
      modeStr = "Area";
      height = 130;
    } else {
      icon1 = Icons.swipe;
      icon2 = Icons.exposure_plus_1;
      icon3 = Icons.do_disturb_off_outlined;
      modeStr = "Edit";
      height = 160;
    }
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        margin: EdgeInsets.all(5),
        height: height,
        // width: 50,
        color: Colors.white.withOpacity(0.3),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ActionButton(
                index: 0,
                icon: icon1,
                active: widget.active,
                onPress: () {
                  // _onPress(0);
                  widget.onDrawPress();
                }),
            widget.mode == MODE.edit
                ? ActionButton(
                    index: 1,
                    icon: Icons.my_location_sharp,
                    active: widget.active,
                    onPress: () {
                      // _onPress(0);
                      widget.onMove();
                    })
                : Container(
                    width: 1,
                  ),
            ActionButton(
                index: 2,
                icon: icon2,
                active: widget.active,
                onPress: () {
                  // _onPress(1);
                  widget.onAddPress();
                }),
            ActionButton(
                index: 3,
                icon: icon3,
                active: widget.active,
                onPress: () {
                  // _onPress(2);
                  widget.onRemovePress();
                }),
            SizedBox(
              height: 5,
              width: 1,
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(2),
                child: ElevatedButton(
                  onPressed: widget.onChangeMode,
                  child: Text(
                    modeStr,
                    style: TextStyle(color: Colors.black),
                  ),
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.lightGreen)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
