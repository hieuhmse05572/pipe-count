import 'package:flutter/material.dart';
import 'package:pipe_count/core/models/circle.dart';
import 'package:pipe_count/core/utils/hull.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart' as intl;

class OverImage extends CustomPainter {
  OverImage(
      {this.image,
      @required this.points,
      @required this.hull,
      @required this.tags,
      this.tagSize = 35,
      this.tagConficence = 50,
      this.isShowNumber = true,
      @required this.sizeOfScreen,
      this.color = Colors.white,
      this.background = Colors.blue});
  ui.Image image;
  double tagSize;
  double tagConficence;
  List<Offset> hull;
  List<Offset> points;
  List<Circle> tags;
  bool isShowNumber;
  Size sizeOfScreen;
  Color color;
  Color background;
  final Paint painter = new Paint()
    ..color = Colors.black.withOpacity(0.85)
    ..style = PaintingStyle.fill;

  final Paint circleBorder = Paint()
    ..strokeWidth = 1
    ..color = Colors.red.withOpacity(1)
    ..style = PaintingStyle.stroke;
  final Paint selectedCircle = Paint()
    ..strokeWidth = 3
    ..color = Colors.red.withOpacity(0.4)
    ..style = PaintingStyle.fill;
  final Paint maskPaint = Paint()
    ..blendMode = BlendMode.clear
    ..strokeWidth = 20
    ..strokeCap = StrokeCap.round;
  List<Circle> getTagInnerArea(List<Circle> tags) {
    if (hull.length == 0) return tags;
    List<Circle> result = List();
    for (int i = 0; i < tags.length; i++) {
      if (Hull.isInside(hull, Offset(tags[i].pos.dx, tags[i].pos.dy))) {
        result.add(tags[i]);
      }
    }
    return result;
  }

  List<Circle> sortByDx(List<Circle> tags) {
    print('size: ');
    print(tags.length);
    Comparator<Circle> sortByDx = (a, b) => a.pos.dx.compareTo(b.pos.dx);
    List<Circle> temp = new List();
    List<Circle> sorted = new List();
    // if(tags.isEmpty) return new List();
    temp.add(tags[0]);
    // sorted.addAll(temp);
    for (int i = 0; i < tags.length - 1; i++) {
      // print(tags[i].pos);
      if (tags[i + 1].pos.dy - 50 > tags[i].pos.dy) {
        temp.sort(sortByDx);
        print(temp.length);
        sorted.addAll(temp);
        temp.clear();
      } else {
        // temp.add(tags[i]);
      }
      temp.add(tags[i + 1]);
    }
    // sorted.add(tags.last);
    // tags = (sorted);
    return sorted;
  }

  void drawTextCenter(
      Canvas context, int num, double x, double y, double size) {
    num += 1;
    TextStyle style = TextStyle(
      color: color,
      fontSize: size,
      fontWeight: FontWeight.bold,
    );
    Size sizeOfText = getSizeOfText("$num", style);
    TextSpan span = new TextSpan(style: style, text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context,
        new Offset(x - sizeOfText.width / 2, y - sizeOfText.height / 2));
  }

  void drawDate(
      Canvas context, String num, double maxsize, double y, double size) {
    TextStyle style = TextStyle(
      color: Colors.black,
      fontSize: size,
      fontWeight: FontWeight.bold,
    );
    Size sizeOfText = getSizeOfText("$num", style);
    TextSpan span = new TextSpan(style: style, text: "$num");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(maxsize - sizeOfText.width - 20, y));
  }

  void drawText(Canvas context, String str, double x, double y, double size) {
    TextStyle style = TextStyle(
      color: Colors.black,
      fontSize: size,
      fontWeight: FontWeight.bold,
    );
    TextSpan span = new TextSpan(style: style, text: "$str");
    TextPainter tp = new TextPainter(
        text: span,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr);
    tp.layout();
    tp.paint(context, new Offset(x, y));
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  //
  // void paintImage(
  //     ui.Image image, Rect outputRect, Canvas canvas, Paint paint, BoxFit fit) {
  //   final Size imageSize =
  //   Size(image.width.toDouble(), image.height.toDouble());
  //   final FittedSizes sizes = applyBoxFit(fit, imageSize, outputRect.size);
  //   final Rect inputSubrect =
  //   Alignment.center.inscribe(sizes.source, Offset.zero & imageSize);
  //   final Rect outputSubrect =
  //   Alignment.center.inscribe(sizes.destination, outputRect);
  //   canvas.drawImageRect(image, inputSubrect, outputSubrect, paint);
  // }

  void drawHull(Canvas canvas, Paint mask) {
    if (hull.length > 0) {
      Path path = new Path();
      path.moveTo(hull[0].dx, hull[0].dy);
      for (int i = 0; i < hull.length - 1; i++) {
        if (hull[i] != null && hull[i + 1] != null) {
          // canvas.drawLine(points[i], points[i + 1], maskPaint);
          path.lineTo(hull[i].dx, hull[i].dy);
        }
      }
      path.close();
      canvas.drawPath(path, mask);
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    double width = sizeOfScreen.width;
    double height = sizeOfScreen.height;
    final Paint circleFill = Paint()
      ..strokeWidth = 3
      ..color = background
      ..style = PaintingStyle.fill;
    final Paint circleFillAdd = Paint()
      ..strokeWidth = 3
      ..color = Colors.green
      ..style = PaintingStyle.fill;
    final Paint blackBackground = new Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;
    // paintImage(image, Rect.fromLTWH(0, 0, width, height), canvas, Paint(),
    //     BoxFit.contain);
    // canvas.drawRect(Rect.fromLTWH(0, 0, width, height), Paint()..color= Colors.blue);
    // canvas.drawImage(image, Offset.zero, Paint());
    tags = getTagInnerArea(tags);

    if (image != null) {
      final df = new intl.DateFormat('dd-MM-yyyy hh:mm a');
      int myvalue = new DateTime.now().millisecondsSinceEpoch;
      var formattedDate =
          (df.format(new DateTime.fromMillisecondsSinceEpoch(myvalue)));
      canvas.drawImage(image, Offset.zero, Paint());
      Rect bottomRect = Rect.fromLTWH(0, height, width, 200);
      canvas.drawRect(bottomRect, blackBackground);
      drawText(canvas, "Kết quả: ${tags.length}", 20, height, tagSize * 1.5);
      drawDate(canvas, formattedDate, width, height, tagSize * 1.5);
    }
    for (int i = 0; i < tags.length; i++) {
      if (tags[i].confidence * 100 >= tagConficence) {
        Paint p = tags[i].isSelected ? selectedCircle : circleFill;
        if (tags[i].type == 2) {
          canvas.drawCircle(tags[i].pos, tagSize, circleFillAdd);
        } else if (tags[i].type == 1)
          canvas.drawCircle(tags[i].pos, tagSize, p);
        canvas.drawCircle(tags[i].pos, tagSize, circleBorder);
        if (isShowNumber)
          drawTextCenter(
              canvas, i, tags[i].pos.dx, tags[i].pos.dy, tagSize - 5);
      }
    }
    if (points.length > 0) {
      canvas.saveLayer(Rect.fromLTWH(0, 0, width, height), Paint());
      canvas.drawRect(Rect.fromLTWH(0, 0, width, height), painter);

      drawHull(canvas, maskPaint);
      for (int i = 0; i < points.length - 1; i++) {
        if (points[i] != null && points[i + 1] != null) {
          canvas.drawLine(points[i], points[i + 1], maskPaint);
        }
      }
      canvas.restore();
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
