// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: public_member_api_docs

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:pipe_count/ui/views/splash_view.dart';

class CaptureScreen extends StatefulWidget {
  final String imagePath;
  CaptureScreen({this.imagePath});
  @override
  _CaptureScreenState createState() {
    return _CaptureScreenState();
  }
}

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

class _CaptureScreenState extends State<CaptureScreen>
    with WidgetsBindingObserver {
  String imagePath;
  ui.Image image;
  int currentTab = 0;

  @override
  void initState() {
    super.initState();
    imagePath = widget.imagePath;
    init();
    // WidgetsBinding.instance.addObserver(this);
  }

  Future<Null> init() async {
    // final ByteData data = await rootBundle.load('assets/images/bg.jpg');
    final data = await readFileByte(imagePath);
    image = await loadImage(Uint8List.view(data.buffer));
    print(image.height);
    print(image.width);
  }

  Future<Uint8List> readFileByte(String filePath) async {
    Uri myUri = Uri.parse(filePath);
    File f = new File.fromUri(myUri);
    Uint8List bytes;
    await f.readAsBytes().then((value) {
      bytes = Uint8List.fromList(value);

      print('reading of bytes is completed');
    }).catchError((onError) {
      print('Exception Error while reading audio from path:' +
          onError.toString());
    });
    return bytes;
  }

  Future<ui.Image> loadImage(List<int> img) async {
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(img, (ui.Image img) {
      return completer.complete(img);
    });
    return completer.future;
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          setState(() {
            imagePath = null;
          });
          Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (context, animation, anotherAnimation) {
                return SplashView();
              },
              transitionDuration: Duration(milliseconds: 700),
              transitionsBuilder:
                  (context, animation, anotherAnimation, child) {
                animation = CurvedAnimation(
                    curve: Curves.fastOutSlowIn, parent: animation);
                return Align(
                    child: FadeTransition(
                  opacity: animation,
                  child: child,
                ));
              }));
          return true;
        },
        child: Scaffold(
          key: _scaffoldKey,
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(0.0),
              child: AppBar(
                automaticallyImplyLeading: false, // hides leading widget
                flexibleSpace: Container(),
              )),
          body: FittedBox(
              child: SizedBox(
            width: image.width.toDouble(),
            height: image.height.toDouble(),
            child: Stack(
              children: <Widget>[
                InteractiveViewer(
                    boundaryMargin: EdgeInsets.all(50),
                    child: Image.file(File(imagePath)))

                // Positioned(
                //   bottom: 0,
                //   left: 0,
                //   right: 0,
                //   child: _controlBottomWidget(),
                // ),
                // Positioned(bottom: 150, left: 90, child: _areaWidgetAnimation())
              ],
            ),
          )),
        ));
  }
}
