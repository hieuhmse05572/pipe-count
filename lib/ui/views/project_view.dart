// import 'package:expandable/expandable.dart';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pipe_count/core/expandable.dart';
import 'package:pipe_count/core/helper/groupHelper.dart';
import 'package:pipe_count/core/helper/projectHelper.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/models/type.dart';
import 'package:pipe_count/core/repo/groupApi.dart';
import 'package:pipe_count/core/utils/sharedPref.dart';
import 'package:pipe_count/ui/views/splash_view.dart';
import 'package:pipe_count/ui/views/type_camera.dart';
import 'package:pipe_count/ui/widgets/button.dart';
import 'package:pipe_count/ui/widgets/dialog.dart';
import 'package:toast/toast.dart';

class ProjectsView extends StatefulWidget {
  @override
  _ProjectsViewState createState() => _ProjectsViewState();
}

class _ProjectsViewState extends State<ProjectsView> {
  List<Project> projects = new List();
  Widget ss = Container();
  bool isLoading = true;
  final projectHelper = ProjectHelper();

  TextEditingController textEditingController = new TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    reload();
  }

  void reload(){
    projectHelper.getProjects("").then((value) async {
      // await new Future.delayed(new Duration(milliseconds: 1000), ()
      // {
      setState(() {
        projects = value;
        isLoading = false;
        // });
      });
    });
  }

  void deleteProjectAt(int projectId) {
    setState(() {
      isLoading = true;
    });
    projectHelper.deleteProjectById(projectId).then((value){
      reload();
    });
  }

  void removeType() {
    setState(() {
    });
  }
  Future<void> _pullRefresh() async {
    reload();
  }
  @override
  Widget build(BuildContext context) {
    String userName = SharePref.prefs.getString('userName');

    String date1 = DateFormat("yyyy/MM/dd").format(DateTime.now());
    // List<String> projectID = SharePref.prefs.getStringList('projectID');
      List<Widget> ws = new List();
      if(projects != null)
      projects.forEach((element) {
        ws.add(
          ArticleWidget(
            project: element,
            onRemovePress: () {
              deleteProjectAt(element.id);
              },
            onAddPress: null,
            onRemoveTypePress: removeType,
          ),
        );
      });
      ss = Column(
        mainAxisSize: MainAxisSize.min,
        children: ws,
      );

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                margin: EdgeInsets.all(10),
                child: TextField(
                  controller: textEditingController,
                  onChanged: (value) {
                    projectHelper.getProjects(value).then((value) async {
                      setState(() {
                        projects = value;
                      });
                    });
                  },
                  style: TextStyle(color: Colors.black),
                  decoration: InputDecoration(
                      hintText: "Enter type to search",
                      labelText: "Search",
                      fillColor: Colors.white,
                      // filled: true,
                      focusColor: Colors.white,
                      suffixIcon: Icon(
                        Icons.search,
                        color: Colors.black,
                      ),
                      border: OutlineInputBorder()),
                )),
            isLoading ?  Expanded(child: innerLoading())
           : Expanded(
              child: RefreshIndicator(
                onRefresh: _pullRefresh,
                child: ws.length !=0 ? ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            date1,
                            style: TextStyle(fontSize: 13, color: Colors.grey),
                            // softWrap: true,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    ss,
                  ],
                ) : Container(
                  color: Colors.white,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(

                          'assets/images/empty-screen.png', height: 200, width: 200,),
                        Text("Opp!. It's Empty", style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic, fontWeight: FontWeight.w300),)
                      ],
                    ),
                  ),),
              ),
            ),
          ],
        ),
        appBar: AppBar(
          title: Text('${userName}'),
          leading:Icon(Icons.person),
          actions: [
            IconButton(
              icon: Icon(Icons.add_box),
              onPressed: () async {
                String name = await DialogUtil.showAddDialog(context);
                if (name != "-1" && name != "") {
                  setState(() {
                    isLoading = true;
                  });
                  String dateStr = new DateTime.now().toIso8601String();
                  Project newProject = new Project(0, name, dateStr, userName, 0);
                  projectHelper.createProject(newProject).then((value) {
                    reload();
                  });
                }
              },
            )
          ],
        ),
      ),
    );
  }
  Widget innerLoading() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CupertinoActivityIndicator(
            radius: 14,
          ),
          SizedBox(
            height: 6,
            width: 1,
          ),
          Container(alignment: Alignment.center, child: Text('Loading...'))
        ],
      ),
    );
  }
}

class ArticleWidget extends StatefulWidget {
  final Function onRemovePress;
  final Function onRemoveTypePress;
  final Function onAddPress;
  final Project project;
  ArticleWidget(
      {@required this.onRemovePress,
      @required this.onRemoveTypePress,
      @required this.onAddPress,
      @required this.project});

  @override
  _ArticleWidgetState createState() => _ArticleWidgetState();
}

class _ArticleWidgetState extends State<ArticleWidget> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    groupHelper.getGroups("${widget.project.id}").then((value) {
      setState(() {
        groups = value;
      });
    });
  }
  final groupHelper  = GroupHelper();
  List<Group> groups = new List();
  @override
  Widget build(BuildContext context) {
    int total = 0;
    //String date = DateFormat("hh:mm").format(DateTime.now());
    List<Widget> ws = new List();
    Widget ss = Container();
    if(groups != null) {
      print(groups.length);
      groups.forEach((element) {
        print(element.name);
        total+= element.total;
        ws.add(
          listTitle(context, element),
        );
      });
      ss = Column(
        mainAxisSize: MainAxisSize.min,
        children: ws,
      );
    }
    return Container(
      padding: const EdgeInsets.all(1.0),
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(
          color: Colors.blueGrey.withOpacity(0.3),
          width: 1,
        ),
      )),
      child: ExpandablePanel(
        iconColor: Colors.indigo,
        header: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.project.name,
              style: TextStyle(fontSize: 15),
            ),

            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButton(
                    enableFeedback: true,
                    color: Colors.red,
                    // splashColor: Colors.indigo,
                    icon: Icon(
                      Icons.delete_forever,
                      size: 20,
                    ),
                    onPressed: () async {
                      String userName = SharePref.prefs.getString('userName');
                      if(userName.trim() != widget.project.created_by.trim()) {
                        Toast.show("You can't delete this project", context,
                            duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                        return;
                      }
                      String status = await DialogUtil.showConfirmDialog(
                        context,
                        'Delete this Project',
                        'Do you want to delete this project?',
                      );
                      if (status == "1") {
                        widget.onRemovePress();
                      }
                    }),
                IconButton(
                    enableFeedback: true,
                    // splashColor: Colors.indigo,
                    icon: Icon(Icons.add, size: 20),
                    onPressed: () async {
                      TypeSteel input =
                          await DialogUtil.showListTypeDialog(context);
                      if (input != null) {
                        String dateStr = new DateTime.now().toIso8601String();
                        Group newGroup = new Group(0, input.name, widget.project.id, input.id, 0, dateStr , "asdf", 0);
                        groupHelper.createGroup(newGroup).then((value) {
                          // setState(() {
                          //   groups.add(newGroup);
                          //   // list = list;
                          // });
                          groupHelper.getGroups("${widget.project.id}").then((value) {
                            setState(() {
                              groups = value;
                            });
                          });
                        });

                        // String status =
                        //     await DialogUtil.showChooseImage(context, newGroup, widget.project);
                      } else {}
                    })
              ],
            )
            // Text(
            //   "Total: 12",
            //   style: TextStyle(fontSize: 15, color: Colors.blueGrey),
            //   // softWrap: true,
            //   maxLines: 1,
            //   overflow: TextOverflow.ellipsis,
            // ),
          ],
        ),
        expanded: Container(
          padding: EdgeInsets.only(left: 35, right: 20),
          child: Column(
            children: [
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // Text(
                  //   date,
                  //   style: TextStyle(fontSize: 15),
                  //   // softWrap: true,
                  //   maxLines: 1,
                  //   overflow: TextOverflow.ellipsis,
                  // ),
                  Text(
                    "Total: ${total}",
                    style: TextStyle(fontSize: 15, color: Colors.indigo),
                  )
                ],
              ),
              ss
            ],
          ),
        ),
        tapHeaderToExpand: true,
        hasIcon: true,
      ),
    );
  }

  Widget listTitle(BuildContext context, Group group) {
    // void removeTypeAt(String name){
    //   projectName.removeWhere((element) {
    //     if (element == name) return true;
    //     return false;
    //   });
    //   SharePref.prefs.setStringList('projectName', projectName);
    //   setState(() {
    //     projectName = projectName;
    //   });
    // }
    return Dismissible(
      // background: Container(
      //     child: Text('Remove', style: TextStyle(color: Colors.white),),
      //     color: Colors.red),
      background: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(left: 20.0),
        color: Colors.redAccent,
        child: Icon(Icons.delete, color: Colors.white),
      ),
      secondaryBackground: Container(
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20.0),
        color: Colors.redAccent,
        child: Icon(Icons.delete, color: Colors.white),
      ),
      confirmDismiss: (direction) async {

        String userName = SharePref.prefs.getString('userName');
        if(userName.trim() != widget.project.created_by.trim()){
          Toast.show("You can't delete this group", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
           return false;
        }
        String status = await DialogUtil.showConfirmDialog(
          context,
          'Delete this types',
          'Do you want to delete this types?',
        );
        if (status == "1")
          return true;
        else
          return false;
      },
      /*New*/
      // direction: (textController.text.isEmpty
      //     ? DismissDirection.horizontal
      //     : DismissDirection.startToEnd
      // ),
      onDismissed: (direction) async {
          await groupHelper.deleteGroupById(group.id);
          groupHelper.getGroups("${widget.project.id}").then((value) {
            setState(() {
              groups = value;
            });
          });

      },
      key: Key("${group.id}"),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => TypeView(
                      group: group,
                     project : widget.project
                    )),
          );
        },
        child: Container(
          margin: EdgeInsets.only(top: 10, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Icons.folder_open_outlined,
                    color: Colors.indigo,
                  ),
                  SizedBox(
                    height: 1,
                    width: 10,
                  ),
                  Text('${group.name}'),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  // Container(
                  //   height: 30,
                  //   child: IconButton(
                  //       enableFeedback: true,
                  //       color: Colors.red,
                  //       // splashColor: Colors.indigo,
                  //       icon: Icon(
                  //         Icons.delete_forever,
                  //         size: 20,
                  //       ),
                  //       onPressed: () async {
                  //         String status =
                  //         await DialogUtil.showConfirmDialog(context, 'Delete this types', 'Do you want to delete this types?',);
                  //         if (status == "1") {
                  //           print('remove');
                  //           List<String> list = SharePref.prefs.getStringList(widget.title);
                  //           print(list);
                  //           print(title);
                  //           list.removeWhere((element) {
                  //             if (element == title) return true;
                  //             return false;
                  //           });
                  //           print(list);
                  //
                  //           SharePref.prefs.setStringList(widget.title, list);
                  //           widget.onRemoveTypePress();
                  //           setState(() {
                  //             // list = list;
                  //           });
                  //         }
                  //       }),
                  // ),
                  Text(
                    '${group.total}',
                    style: TextStyle(color: Colors.blueGrey),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
