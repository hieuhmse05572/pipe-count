import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pipe_count/core/helper/loginHelper.dart';
import 'package:pipe_count/core/utils/sharedPref.dart';
import 'package:pipe_count/ui/views/project_view.dart';

class SplashView extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}
class _SplashState extends State<SplashView> {
  TextEditingController textEditingController = new TextEditingController();
  bool isLogined = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharePref.init();
    Future.delayed(Duration.zero, () async {
      if (await Permission.camera.request().isGranted) {}
      if (await Permission.storage.request().isGranted) {}
    });
    login();
  }

  @override
  Widget build(BuildContext context) {
    // SizeConfig().init(context);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.indigo,
      // resizeToAvoidBottomPadding: false,
      body: Column(
        children: <Widget>[
            Expanded(child: buildLogo()),
            isLogined ?  Expanded(child: buildInput()) : Container()
        ],
      ),
    );
  }
  Widget buildInput(){
    return ClipPath(
      clipper: WaveClipperTwo(
          flip: true,
          reverse: true
      ),
      child: Container(
        padding: EdgeInsets.only(top: 80, right: 40, left: 40),
        // width: 500  ,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Text('Get Started with PipeCount', style: TextStyle(fontSize: 35,
                  fontFamily: 'StintUltraCondensed'
              ),),
            ),
            Container(
                height: 50,
                // color: Colors.black12,
                margin: EdgeInsets.all(10),
                child: TextField(
                  controller: textEditingController,
                  onChanged: (value) {
                  },
                  style: TextStyle(color: Colors.indigo),
                  decoration: InputDecoration(
                      
                      focusedBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.indigo, width: 1.00, ),
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: const BorderSide(color: Colors.indigo, width: 1.5),
                      ),
                      // hintText: "Enter your name",
                      labelText: "Enter your name",
                      // labelStyle: TextStyle(color: Colors.grey),
                      fillColor: Colors.black,
                      // filled: true,
                      focusColor: Colors.black,
                      // suffixIcon: IconButton(icon: Icon(
                      //   Icons.arrow_forward,
                      //   color: Colors.black,
                      // ), onPressed: () {
                      //
                      // },
                      // ),
                      border: OutlineInputBorder()
                  ),
                )),
           Column(
             mainAxisSize: MainAxisSize.min,
             children: [
               // SizedBox(
               //   height: 5, width: 1,
               // ),
               Text('Enter your name to start count pipes',
                 style: TextStyle(fontSize: 13),
               ),
               SizedBox(
                 height: 30, width: 1,
               ),
               OutlineButton(
                 disabledBorderColor: Colors.white,
                 highlightedBorderColor: Colors.white,
                 onPressed: (){
                   String name = textEditingController.text;
                   SharePref.prefs.setString('userName', name);
                   login();

                 },
                 child: Text('Get started', style: TextStyle(color: Colors.indigo),),),
               SizedBox(
                 height: 30, width: 1,
               ),
             ],
           ),
            Container(
              height: 20,
              child: Text('@Copyright by NSMV, 2021', style: TextStyle(fontSize: 11,
                  // fontFamily: 'StintUltraCondensed'
              ),),
            ),
          ],
        ),
      ),
    );
  }
  Widget buildLogo(){
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 1),
              borderRadius: BorderRadius.circular(50),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("assets/icon/icon.png"),
              )),
          width: 100,
          height: 100,
        ),

        Text(
          'PipeCount',
          style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontFamily: 'KirangHaerang'
          ),
        ),
      ],
    );
  }

  void login() {
    SharePref.init();
    LoginHelper().login("00001", "12345678").then((value) async {
      String userName = await SharePref.getString('userName');
      // userName = null;
      setState(() {
        if(userName == null){
          isLogined = true;
        }else{
          Navigator.of(context).pushReplacement(PageRouteBuilder(
              pageBuilder: (context, animation, anotherAnimation) {
                return ProjectsView();
              },
              transitionDuration: Duration(milliseconds: 0),
              transitionsBuilder: (context, animation, anotherAnimation, child) {
                animation =
                    CurvedAnimation(curve: Curves.fastOutSlowIn, parent: animation);
                return Align(
                    child: FadeTransition(
                      opacity: animation,
                      child: child,
                    ));
              }));
        }
      });
    });
  }
}
