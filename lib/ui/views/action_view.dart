import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pipe_count/core/enum/mode.dart';
import 'package:pipe_count/core/enum/viewstate.dart';
import 'package:pipe_count/getIt.dart';
import 'package:pipe_count/ui/controllers/action_provider.dart';
import 'package:pipe_count/ui/widgets/button_group.dart';

class OnShow extends StatefulWidget {
  final Function onCountPress;
  final int result;
  final Function onClearPress;
  final Function onSavePress;
  final Function onBackPress;
  OnShow({this.onCountPress, this.onSavePress, this.onClearPress,this.onBackPress, this.result});

  @override
  _OnShowState createState() => _OnShowState();
}

class _OnShowState extends State<OnShow> {
  final actionProvider = getIt<ActionProvider>();
  int active;

  @override
  Widget build(BuildContext context) {
    active = actionProvider.active;
    // setState(() {
    //   active = actionProvider.active;
    // });
    return Stack(
      children: [
        BottomLeftGroupButton(
          onBackPress: () {
            SystemChrome.setPreferredOrientations(
                [DeviceOrientation.portraitUp]).then((_) {
              widget.onBackPress();
            });
          },
          onHidePress: () {
            actionProvider.changeActionView(BUTTON.Hide);
          },
        ),
        ResultButton(
          result: widget.result,
        ),
        TopLeftGroupButton(
          onCountPress: widget.onCountPress,
          onSavePress: widget.onSavePress,
        ),
        StreamBuilder<MODE>(
            initialData: actionProvider.lastMode,
            stream: actionProvider.action,
            builder: (context, snapshot) {
              return snapshot.data == MODE.draw
                  ? ActionButtons(
                      onDrawPress: () {
                        setState(() {
                          active = 0;
                        });
                        actionProvider.changeDrawAction(DRAW.move, 0);
                      },
                      onAddPress: () {
                        setState(() {
                          active = 2;
                        });
                        actionProvider.changeDrawAction(DRAW.draw, 2);
                      },
                      onRemovePress: () {
                        widget.onClearPress();
                        setState(() {
                          active = -1;
                        });
                        actionProvider.changeDrawAction(DRAW.clear, -1);
                      },
                      mode: MODE.draw,
                      active: active,
                      onChangeMode: () {
                        setState(() {
                          active = -1;
                        });
                        actionProvider.changeMode(MODE.edit);
                        actionProvider.resetAction();
                      },
                    )
                  : ActionButtons(
                      onDrawPress: () {
                        setState(() {
                          active = 0;
                        });
                        actionProvider.changeEditAction(EDIT.move, 0);
                      },
                      onMove: () {
                        setState(() {
                          active = 1;
                        });
                        actionProvider.changeEditAction(EDIT.drag, 1);
                      },
                      onAddPress: () {
                        setState(() {
                          active = 2;
                        });
                        actionProvider.changeEditAction(EDIT.add, 2);
                      },
                      onRemovePress: () {
                        setState(() {
                          active = 3;
                        });
                        actionProvider.changeEditAction(EDIT.remove, 3);
                      },
                      mode: MODE.edit,
                      active: active,
                      onChangeMode: () {
                        setState(() {
                          active = -1;
                        });
                        actionProvider.changeMode(MODE.draw);
                        actionProvider.resetAction();
                      },
                    );
            })
      ],
    );
  }
}

class OnHide extends StatelessWidget {
  final actionProvider = getIt<ActionProvider>();

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.bottomLeft,
        child: Container(
          height: 35,
          width: 70,
          margin: EdgeInsets.all(5),
          child: ElevatedButton(
            onPressed: () {
              actionProvider.changeActionView(BUTTON.Show);
            },
            child: Icon(
              Icons.lock_open,
              color: Colors.black,
              size: 31,
            ),
            style: ButtonStyle(
                overlayColor: MaterialStateProperty.all<Color>(Colors.blue),
                backgroundColor: MaterialStateProperty.all<Color>(
                    Colors.white.withOpacity(0.7))),
          ),
        ));
  }
}

class ActionView extends StatelessWidget {
  final Function onCountPress;
  final Function onClearPress;
  final Function onSavePress;
  final Function onBackPress;
  final int result;
  ActionView(
      {@required this.onCountPress,
      @required this.onSavePress,
      @required this.onClearPress,
      @required this.onBackPress,
      this.result});
  final actionProvider = getIt<ActionProvider>();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BUTTON>(
        initialData: BUTTON.Show,
        stream: actionProvider.actionView,
        builder: (context, snapshot) {
          return snapshot.data == BUTTON.Show
              ? OnShow(
                  onCountPress: onCountPress,
                  onClearPress: onClearPress,
                  onSavePress: onSavePress,
                  onBackPress: onBackPress,
                  result: result)
              : OnHide();
        });
  }
}
