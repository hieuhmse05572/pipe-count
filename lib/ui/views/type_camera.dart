import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/utils/ImageUtil.dart';
import 'package:pipe_count/ui/views/project_view.dart';
import 'package:pipe_count/core/helper/ImageHelper.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/utils/sharedPref.dart';
import 'package:pipe_count/ui/widgets/dialog.dart';
import 'package:pipe_count/core/models/image.dart' as im;
import 'package:pipe_count/ui/views/_view.dart';
import 'package:toast/toast.dart';

import 'project_view.dart';

class TypeView extends StatefulWidget {
  final Group group;
  final Project project;
  TypeView({this.group, this.project});
  @override
  _TypeViewState createState() => _TypeViewState();
}

class _TypeViewState extends State<TypeView> {
  List<Widget> ws = new List();
  final imageHelper = ImageHelper();
  bool isLoading = false;
  int total = 0;
  Future<void> _pullRefresh() async {
    refresh();
  }
  void refresh(){

    total = 0;
    imageHelper.getImages(widget.group.id, widget.project.id, 0, 10000).then((value) {
      setState(() {
        print('refresh image');
        print(value.length);
        ws = new List();
        value.forEach((element) {
          total += element.total;
          ws.add(listTitle(image: element, group: widget.group, project: widget.project , onRemove: () async {
            String userName = SharePref.prefs.getString('userName');
            if(userName.trim() != widget.project.created_by.trim()){
              Toast.show("You can't delete this image", context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
               return;
            }
            String status =
                await DialogUtil.showConfirmDialog(context, 'Delete this image', 'Do you want to delete this image?',);
            if(status != "-1"){
              isLoading = true;
              ImageHelper().deleteImageById(element.id).then((value) {
                refresh();
              });
            }
          },));
        });
        isLoading = false;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    total = 0;
    isLoading = true;
    refresh();
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          leading:  IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () async {
              Navigator.of(context).pushReplacement(PageRouteBuilder(
                  pageBuilder: (context, animation, anotherAnimation) {
                    return SafeArea(
                      child: ProjectsView(),
                    );
                  },
                  transitionDuration: Duration(milliseconds: 100),
                  transitionsBuilder:
                      (context, animation, anotherAnimation, child) {
                    animation = CurvedAnimation(
                        curve: Curves.fastOutSlowIn, parent: animation);
                    return Align(
                        child: FadeTransition(
                          opacity: animation,
                          child: child,
                        ));
                  }));
            },
          ),
          actions: [
            IconButton(
              icon: Icon(Icons.camera_alt),
              onPressed: () async {
                String status =
                    await DialogUtil.showChooseImage(context, widget.group, widget.project);
              },
            )
          ],
          title: Text("${widget.group.name}"),
        ),
        body: Container(
          child: Column(
            children: [
              Container(
                decoration:
                    BoxDecoration(border: Border(bottom: BorderSide(width: 1))),
                margin: EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      'Total: ${total}',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              isLoading ? innerLoading():  Expanded(
                child: RefreshIndicator(
                  onRefresh: _pullRefresh,
                  child: ws.length != 0 ? ListView(
                      children: ws) : Container(
                    color: Colors.white,
                    child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(

                            'assets/images/empty-screen.png', height: 200, width: 200,),
                        Text("Opp!. It's Empty", style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic, fontWeight: FontWeight.w300),)
                      ],
                    ),
                  ),) ,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget innerLoading() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CupertinoActivityIndicator(
            radius: 14,
          ),
          SizedBox(
            height: 6,
            width: 1,
          ),
          Container(alignment: Alignment.center, child: Text('Loading...'))
        ],
      ),
    );
  }
}

class listTitle extends StatelessWidget {
  final im.Image image;
  final Function onRemove;
  final Group group;
  final Project project;

  listTitle({this.image,this.group, @required this.onRemove, this.project});
  @override
  Widget build(BuildContext context) {
    void _onLoading(String title) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return dialogMessage(
            context,
            title,
            "Please wait !!!",
          );
        },
      );
    }
    var path = 'https://pipe.nsmv.com.vn/api/file/images/low/'+image.url ;
    var originImage = 'https://pipe.nsmv.com.vn/api/file/images/'+image.url;
    if(image.url == 'null'){
      path = 'https://www.thermaxglobal.com/wp-content/uploads/2020/05/image-not-found.jpg';
    }
    return InkWell(
      onTap: () async {
        _onLoading( "Loading Image...");
        final imageFromNet = await ImageUtil.loadImageFromNet(originImage);
        Navigator.pop(context);
        if (imageFromNet == null) {
          Toast.show("ERROR: Timeout", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
          // _timer.run(() {
          // });
        }

        Navigator.of(context).pushReplacement(PageRouteBuilder(
            pageBuilder: (context, animation, anotherAnimation) {
              return SafeArea(
                child: Game(
                    imageFromNetwork: imageFromNet,
                    group: group,
                    image: image,
                    project: project,
                    edit: true,
                ),
              );
            },
            transitionDuration: Duration(milliseconds: 0),
            transitionsBuilder:
                (context, animation, anotherAnimation, child) {
              animation = CurvedAnimation(
                  curve: Curves.fastOutSlowIn, parent: animation);
              return Align(
                  child: FadeTransition(
                    opacity: animation,
                    child: child,
                  ));
            }));
      },
      child: Container(
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.only(bottom: 5),
        child: Row(
          children: [
            Container(height: 110, width: 110, child:
            // Image.file(new File(image.url))
            //     Image.network(path)
            // CachedNetworkImage(
            //   imageUrl: path,
            //   placeholder: (context, url) =>
            //    Container(
            //        alignment: Alignment.center,
            //        width: 50,
            //        height: 50,
            //        child: Center(child: CircularProgressIndicator())),
            //   errorWidget: (context, url, error) => Icon(Icons.error),
            // ),
             Image.network(
             path,
              loadingBuilder: (ctx, child, loading) {
               if(loading == null) return child;
              return Container(
                  alignment: Alignment.center,
                  width: 150,
                  height: 150,
                  child: Center(child: CircularProgressIndicator()));},
              errorBuilder: (ctx, o, n) {
                print(o);
                return Icon(Icons.error);
              },
             )
                ),
            Expanded(
              child: Container(
                height: 113,
                margin: EdgeInsets.only(left: 5),
                padding: EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Total: ${image.total}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          Row(
                            children: [
                              IconButton(
                                icon:
                                    Icon(Icons.delete_forever, color: Colors.red),
                                onPressed: onRemove
                              ),
                              IconButton(
                                icon: Icon(Icons.edit, color: Colors.indigo),
                                onPressed: () async {
                                  _onLoading( "Loading Image...");
                                  final imageFromNet = await ImageUtil.loadImageFromNet(originImage);
                                  Navigator.pop(context);

                                  if (imageFromNet == null) {
                                    Toast.show("ERROR: Timeout", context,
                                        duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                                    // _timer.run(() {
                                    // });
                                  }

                                  Navigator.of(context).pushReplacement(PageRouteBuilder(
                                      pageBuilder: (context, animation, anotherAnimation) {
                                        return SafeArea(
                                          child: Game(
                                              imageFromNetwork: imageFromNet,
                                              group: group,
                                              project: project,
                                              image: image,
                                            edit: true,
                                          ),
                                        );
                                      },
                                      transitionDuration: Duration(milliseconds: 0),
                                      transitionsBuilder:
                                          (context, animation, anotherAnimation, child) {
                                        animation = CurvedAnimation(
                                            curve: Curves.fastOutSlowIn, parent: animation);
                                        return Align(
                                            child: FadeTransition(
                                              opacity: animation,
                                              child: child,
                                            ));
                                      }));
                                },
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Text(
                      "Missing: ${image.missing}",
                      style: TextStyle(color: Colors.indigo),
                    ),
                    Text(
                      "Error: ${image.error}",
                      style: TextStyle(color: Colors.red),
                    ),
                    Text(
                      '${image.created_date}',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                    Text(
                      "Created by: ${image.created_by}",
                      style: TextStyle(fontStyle: FontStyle.italic),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        decoration: BoxDecoration(
            border:
                Border(bottom: BorderSide(color: Colors.grey.withOpacity(0.4)))),
      ),
    );
  }
}
