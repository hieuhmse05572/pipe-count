import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:math' as math;
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show SystemChrome, rootBundle;
import 'package:pipe_count/core/models/image.dart' as im;
import 'package:flutter/services.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:pipe_count/core/enum/mode.dart';
import 'package:pipe_count/core/helper/ImageHelper.dart';
import 'package:pipe_count/core/helper/uploadFileHelper.dart';
import 'package:pipe_count/core/models/circle.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/utils/ImageUtil.dart';
import 'package:pipe_count/core/utils/hull.dart';
import 'package:pipe_count/core/utils/sharedPref.dart';
import 'package:pipe_count/getIt.dart';
import 'package:pipe_count/ui/controllers/action_provider.dart';
import 'package:pipe_count/ui/widgets/button_group.dart';
import 'package:pipe_count/ui/widgets/custom_painter.dart';
import 'package:pipe_count/ui/widgets/dialog.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

import 'action_view.dart';
import 'type_camera.dart';

// typedef void _TeamCallback(_Team team);
// typedef void _PieceDataCallback(_PieceData pieceData);

class Game extends StatefulWidget {
  final Group group;
  final Project project;
  final ui.Image imageFromNetwork;
  final bool edit;
  final im.Image image;
  final String imagePathFromLocal;
  Game({
    this.group,
    this.imageFromNetwork,
    this.project,
    this.edit,
    this.image,
    this.imagePathFromLocal,
    Key key,
  }) : super(key: key);

  @override
  _GameState createState() => _GameState();
}

class _GameState extends State<Game> {
  List<Circle> tags = List();
  EDIT edit;
  DRAW draw;
  int missing = 0;
  String fileNameServer = "";
  int error = 0;
  double _imageAspectRatio;
  Uint8List _imageData;
  List<Offset> points = List();
  List<Offset> hull = List();
  final actionProvider = getIt<ActionProvider>();
  int selectedTag = -1;
  final TransformationController _transformationController =
      TransformationController();
  ui.Image image;
  double height;
  double width;
  int rotate = 0;
  GlobalKey _myCanvasKey = new GlobalKey();

  @override
  void dispose() {
    // TODO: implement dispose
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    super.dispose();
    print('dispose');
  }

  Future<bool> getResultCouting() async {
    tags = [];
    if (widget.edit) {
      fileNameServer = widget.image.url;
    } else {
      fileNameServer =
          await UploadFileHelper().uploadFile(widget.imagePathFromLocal);
    }
    List<Circle> circles = await UploadFileHelper().getPosition(fileNameServer);
    if (circles == null) return false;
    setState(() {
      tags.addAll(circles);
    });
    // _tagsStream.add(tags);
    // refresh();
    return true;
  }

  // Load the image in advance in order to get its size.
  void _loadImage() async {
    if (widget.edit) {
      image = widget.imageFromNetwork;
      List<Circle> circles = List();
      if (widget.image.true_tag != '' && widget.image.true_tag != null) {
        String json = "[" + widget.image.true_tag + "]";
        final data = jsonDecode(json);
        if (data != null) {
          for (Map i in data) {
            Circle circle = Circle.fromJson(i);
            circles.add(circle);
          }
        }
      }
      setState(() {
        tags.addAll(circles);
        _imageData = new Uint8List(1);
        _imageAspectRatio = image.width.toDouble() / image.height.toDouble();
      });
    } else {
      final data = await ImageUtil.readFileByte(widget.imagePathFromLocal);
      Uint8List buffer = Uint8List.view(data.buffer);
      image = await ImageUtil.loadImage(buffer);
      setState(() {
        _imageData = buffer;
        _imageAspectRatio = image.width.toDouble() / image.height.toDouble();
      });
    }

    height = image.height.toDouble();
    width = image.width.toDouble();

    if (width > height) {
      SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
    }
  }

  Future<String> ScreenShot(bool isSaveResultImage) async {
    String fileName = "${new DateTime.now().millisecondsSinceEpoch}";
    // if(isSaveOriginalImage) saveOriginalImage(fileName);
    // if(isSaveResultImage) {
    //   ui.Image imagee = await getImage();
    //   ByteData byteData = await imagee.toByteData(
    //       format: ui.ImageByteFormat.png);
    //
    //   final result = await ImageGallerySaver.saveImage(
    //       byteData.buffer.asUint8List(),
    //       name: "${fileName}_result_image",
    //       isReturnImagePathOfIOS: true,
    //       quality: 80);
    //   print(result);
    // }
    int total = getTagInnerArea(tags).length;
    String circleStr = getJsonCircle(tags);
    String dateStr =
        DateTime.now().subtract(Duration(hours: 7)).toIso8601String();
    String userName = SharePref.prefs.getString('userName');
    if (widget.edit) {
      im.Image image = new im.Image(
          widget.image.id,
          "a",
          widget.image.error + error,
          widget.image.missing + missing,
          total,
          widget.image.url,
          dateStr,
          userName,
          0,
          widget.group.id,
          widget.project.id,
          "[]",
          circleStr,
          total - widget.image.total,
      );
      await ImageHelper().updateImage(image);
    } else {
      im.Image image = new im.Image(
          0,
          "a",
          error,
          missing,
          total,
          fileNameServer,
          dateStr,
          userName,
          0,
          widget.group.id,
          widget.project.id,
          "[]",
          circleStr, 0);
      await ImageHelper().createImage(image);
    }
    print('save done');
    // return result['filePath'];
    return '';
  }

  Future<ui.Image> getImage() async {
    ui.PictureRecorder recorder = ui.PictureRecorder();
    Canvas canvas = Canvas(recorder);
    Size sizee = Size(width, height + 200);
    dynamic imageSaver = OverImage(
        image: image,
        points: points,
        hull: hull,
        tags: tags,
        sizeOfScreen: Size(width, height));
    imageSaver.paint(canvas, sizee);
    return recorder.endRecording().toImage(width.floor(), height.floor() + 200);
  }

  void asb() {
    var s = json.encode(tags);
    json.decode(s);
  }

  void _onLoading(String title) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return dialogMessage(
          context,
          title,
          "Please wait !!!",
        );
      },
    );
  }

  String removeLastCharacter(String str) {
    String result = "";
    if ((str != null) && (str.length > 0)) {
      result = str.substring(0, str.length - 1);
    }

    return result;
  }

  List<Circle> getTagInnerArea(List<Circle> tags) {
    if (hull.length == 0) return tags;
    List<Circle> result = List();
    for (int i = 0; i < tags.length; i++) {
      if (Hull.isInside(hull, Offset(tags[i].pos.dx, tags[i].pos.dy))) {
        result.add(tags[i]);
      }
    }
    return result;
  }

  String getJsonCircle(List<Circle> tags) {
    String jsonStr = '';
    for (int i = 0; i < tags.length; i++) {
      jsonStr += tags[i].toJson() + ',';
    }
    jsonStr = removeLastCharacter(jsonStr);
    // jsonStr = jsonStr ;
    return jsonStr;
  }

  List<Circle> sortNumber(List<Circle> tags) {
    Comparator<Circle> sortByDy = (a, b) => a.pos.dy.compareTo(b.pos.dy);
    tags.sort(sortByDy);
    return tags;
  }

  @override
  void initState() {
    super.initState();
    _loadImage();
  }

  @override
  Widget build(BuildContext context) {
    if (_imageData == null || _imageAspectRatio == null) {
      return SizedBox.shrink();
    }

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          backgroundColor: Colors.blueGrey,
          body: Stack(
            children: <Widget>[
              Center(
                child: FittedBox(
                    fit: BoxFit.fill,
                    child: SizedBox(
                        width: width,
                        height: height,
                        child: StreamBuilder<EDIT>(
                            initialData: actionProvider.lastEdit,
                            stream: actionProvider.edit,
                            builder: (context, snapshot) {
                              edit = snapshot.data;
                              return GestureDetector(
                                onTapUp: (TapUpDetails details) {
                                  if (edit == EDIT.drag) {
                                    if (selectedTag >= 0) {
                                      tags[selectedTag].isSelected = false;
                                      selectedTag = -1;
                                      _myCanvasKey.currentContext
                                          .findRenderObject()
                                          .markNeedsPaint();
                                      return;
                                    }
                                  }
                                },
                                onTapDown: (TapDownDetails details) {
                                  final Offset pos = _transformationController
                                      .toScene(details.localPosition);
                                  if (edit == EDIT.add) {
                                    // points.add(pos);
                                    Circle circle =
                                        Circle(2, pos, 1, 35, false);
                                    missing++;
                                    setState(() {
                                      tags.add(circle);
                                      tags = sortNumber(tags);
                                    });
                                  } else if (edit == EDIT.remove) {
                                    tags.removeWhere((element) {
                                      if ((element.pos.dx - pos.dx).abs() <=
                                              35 &&
                                          (element.pos.dy - pos.dy).abs() <= 35)
                                        return true;
                                      return false;
                                    });
                                    error++;
                                    setState(() {
                                      tags = sortNumber(tags);
                                    });
                                  } else if (edit == EDIT.drag) {
                                    selectedTag = tags.indexWhere((element) {
                                      if ((element.pos.dx - pos.dx).abs() <=
                                              35 &&
                                          (element.pos.dy - pos.dy).abs() <= 35)
                                        return true;
                                      return false;
                                    });
                                    if (selectedTag >= 0) {
                                      tags[selectedTag].isSelected = true;
                                      error++;
                                    }
                                  }
                                  _myCanvasKey.currentContext
                                      .findRenderObject()
                                      .markNeedsPaint();
                                },
                                child: StreamBuilder<DRAW>(
                                    initialData: actionProvider.lastDraw,
                                    stream: actionProvider.draw,
                                    builder: (context, snapshot) {
                                      draw = snapshot.data;
                                      return InteractiveViewer(
                                        onInteractionEnd: (detail) {
                                          setState(() {
                                            if (draw == DRAW.draw) {
                                              hull.clear();
                                              hull.addAll(Hull.convexHull(
                                                  points, points.length));
                                            }
                                          });
                                          if (edit == EDIT.drag) {
                                            if (selectedTag >= 0) {
                                              setState(() {
                                                tags[selectedTag].isSelected =
                                                    false;
                                                selectedTag = -1;
                                                tags = sortNumber(tags);
                                              });
                                            }
                                          }
                                          _myCanvasKey.currentContext
                                              .findRenderObject()
                                              .markNeedsPaint();
                                        },
                                        onInteractionStart:
                                            (scaleStartDetails) {},
                                        onInteractionUpdate:
                                            (scaleUpdateDetails) {
                                          if (draw == DRAW.draw) {
                                            points.add(
                                                scaleUpdateDetails.focalPoint);
                                          }
                                          if (edit == EDIT.drag) {
                                            if (selectedTag >= 0) {
                                              setState(() {
                                                Circle selectedCircle =
                                                    tags[selectedTag];
                                                double dx = (scaleUpdateDetails
                                                    .focalPoint.dx);
                                                double dy = (scaleUpdateDetails
                                                    .focalPoint.dy);
                                                selectedCircle.pos =
                                                    Offset(dx, dy);
                                              });
                                            }
                                          }
                                          _myCanvasKey.currentContext
                                              .findRenderObject()
                                              .markNeedsPaint();
                                        },
                                        panEnabled: draw == DRAW.move ||
                                            edit == EDIT.move,
                                        scaleEnabled: draw == DRAW.move ||
                                            edit == EDIT.move,
                                        transformationController:
                                            _transformationController,
                                        child: Stack(
                                          children: <Widget>[
                                            Center(
                                                child: SizedBox(
                                                    width: width,
                                                    height: height,
                                                    child: widget.edit
                                                        ? Image.network(
                                                            'https://pipe.nsmv.com.vn/api/file/images/' +
                                                                widget
                                                                    .image.url)
                                                        : Image.memory(
                                                            _imageData))),
                                            CustomPaint(
                                              key: _myCanvasKey,
                                              painter: OverImage(
                                                  points: points,
                                                  hull: hull,
                                                  tags: tags,
                                                  sizeOfScreen:
                                                      Size(width, height)),
                                            )
                                          ],
                                        ),
                                      );
                                    }),
                              );
                            }))),
              ),
              ActionView(
                result: getTagInnerArea(tags).length,
                onBackPress: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) => TypeView(
                        group: widget.group,
                        project: widget.project,
                      ),
                    ),
                  );
                  //
                  // Navigator.of(context).pushReplacement(PageRouteBuilder(
                  //     pageBuilder: (context, animation, anotherAnimation) {
                  //       return SafeArea(
                  //         child: TypeView(  group: widget.group),
                  //       );
                  //     },
                  //     transitionDuration: Duration(milliseconds: 100),
                  //     transitionsBuilder:
                  //         (context, animation, anotherAnimation, child) {
                  //       animation = CurvedAnimation(
                  //           curve: Curves.fastOutSlowIn, parent: animation);
                  //       return Align(
                  //           child: FadeTransition(
                  //             opacity: animation,
                  //             child: child,
                  //           ));
                  //     }));
                },
                onCountPress: () async {
                  _onLoading("Counting ...");
                  final status = await getResultCouting();
                  Navigator.pop(context);
                  if (status == false) {
                    Toast.show("ERROR: Timeout", context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                    // _timer.run(() {
                    // });
                  }
                },
                onClearPress: () {
                  _transformationController.value = Matrix4.identity();
                  setState(() {
                    hull.clear();
                    points.clear();
                  });
                },
                onSavePress: () async {
                  if (tags.isEmpty) return null;
                  String status = await DialogUtil.showConfirmDialog(
                    context,
                    'Save result image',
                    'Do you want to save result image?',
                  );
                  if (status != "-1") {
                    _onLoading("Saving ...");
                    ScreenShot(true).then((value) {
                      Navigator.pop(context);
                      Toast.show("Save Success", context,
                          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                    });
                  } else {
                    // Navigator.pop(context);
                  }
                },
              )
            ],
          )),
    );
  }
}
