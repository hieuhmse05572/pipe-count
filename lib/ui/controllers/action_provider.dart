import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pipe_count/core/enum/mode.dart';
import 'package:pipe_count/core/enum/viewstate.dart';

class ActionProvider with ChangeNotifier {
  MODE lastMode;
  EDIT lastEdit;
  DRAW lastDraw;
  BUTTON button;
  int active = -1;

  final _actionView = StreamController<BUTTON>.broadcast();
  Stream<BUTTON> get actionView => _actionView.stream;

  final _action = StreamController<MODE>.broadcast();
  Stream<MODE> get action => _action.stream;

  final _draw = StreamController<DRAW>.broadcast();
  Stream<DRAW> get draw => _draw.stream;

  final _edit = StreamController<EDIT>.broadcast();
  Stream<EDIT> get edit => _edit.stream;

  ActionProvider() {
    print("init Action");
  }

  changeActionView(BUTTON button) {
    button = button;
    _actionView.add(button);
  }

  changeMode(MODE mode) {
    lastMode = mode;
    _action.add(mode);

    // notifyListeners();
  }

  resetAction() {
    _draw.add(DRAW.none);
    _edit.add(EDIT.none);
    active = -1;
  }

  changeDrawAction(DRAW draw, int active) {
    this.active = active;
    lastDraw = draw;
    _draw.add(draw);
    changeMode(lastMode);
  }

  changeEditAction(EDIT edit, int active) {
    this.active = active;
    lastEdit = edit;
    _edit.add(edit);
    changeMode(lastMode);
  }
}
