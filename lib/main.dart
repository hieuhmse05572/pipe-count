import 'package:flutter/material.dart';
import 'package:pipe_count/getIt.dart';
import 'ui/views/splash_view.dart';

void main() {
  // setupServiceLocator();
  setup();
  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PipeCount',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: SplashView(),
    );
  }
}
