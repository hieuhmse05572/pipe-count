

import 'package:intl/intl.dart';

class Project {
  int id;
  String name;
  String created_date;
  String created_by;
  int delete_flg;

  Project(
      this.id, this.name, this.created_date, this.created_by, this.delete_flg);

  factory Project.fromJson(Map<String, dynamic> json) {
    String dateStr =  readTimestamp(json["created_date"]);
    return Project(
        json["id"] as int,
        json["name"] as String,
        dateStr,
        json["created_by"] as String,
        0);
  }


   Map toObject(){
    Project project = this;
    return {
      "id": "${project.id}",
      "name": "${project.name}",
      "created_date": "${project.created_date}",
      "created_by": "${project.created_by}",
      "delete_flg": "${project.delete_flg}",
    };
  }
  static Map getBody(String username, String password) {
    return {"userName": "$username", "password": "$password"};
  }
  static String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm a');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 || diff.inSeconds > 0 && diff.inMinutes == 0 || diff.inMinutes > 0 && diff.inHours == 0 || diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' DAY AGO';
      } else {
        time = diff.inDays.toString() + ' DAYS AGO';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
      } else {

        time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
      }
    }
    return time;
  }

}