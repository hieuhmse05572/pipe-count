
class TypeSteel {
  int id;
  String name;


  TypeSteel(this.id, this.name);

  factory TypeSteel.fromJson(Map<String, dynamic> json) {
    return TypeSteel(
        json["id"] as int,
        json["name"] as String);
  }

  Map toObject(){
    TypeSteel type = this;
    return {
      "id": "${type.id}",
      "name": "${type.name}",
    };
  }
}