import 'package:intl/intl.dart';

class Image{
   int id;
   String name;
   int error;
   int missing;
   int total;
   String url;
   String created_date;
   String created_by;
   int delete_flg;
   int group_id;
   int project_id;
   String error_tag;
   String true_tag;
   int temp_count;

   Image(this.id, this.name, this.error,this.missing, this.total, this.url, this.created_date,
      this.created_by, this.delete_flg, this.group_id, this.project_id,  this.error_tag, this.true_tag, this.temp_count);

   factory Image.fromJson(Map<String, dynamic> json) {
      String dateStr =  convertTime(json["created_date"]);
      return Image(
          json["id"] as int,
          json["name"] as String,
          json["error"] as int,
          json["missing"] as int,
          json["total"] as int,
          json["url"] as String,
          dateStr,
          json["created_by"] as String,
          0,
         json["group_id"] as int,
         json["project_id"] as int,
         json["error_tag"] as String,
         json["true_tag"] as String,
         0
      );
   }

   Map toObject(){
      Image image = this;
      return {
         "id": "${image.id}",
         "name": "${image.name}",
         "error": "${image.error}",
         "missing": "${image.missing}",
         "total": "${image.total}",
         "url": "${image.url}",
         "created_date": "${image.created_date}",
         "group_id": "${image.group_id}",
         "project_id": "${image.project_id}",
         "created_by": "${image.created_by}",
         "delete_flg": "${image.delete_flg}",
         "error_tag": "${image.error_tag}",
         "true_tag": "${image.true_tag}",
         "temp_count": "${image.temp_count}",

      };
   }

   static String convertTime(int timestamp) {
      var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
      var format = DateFormat('yyyy-MM-dd HH:mm a');
      var time = format.format(date);
      return time;
   }

   static String readTimestamp(int timestamp) {
      var now = DateTime.now();
      var format = DateFormat('HH:mm a');
      var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
      var diff = now.difference(date);
      var time = '';

      if (diff.inSeconds <= 0 || diff.inSeconds > 0 && diff.inMinutes == 0 || diff.inMinutes > 0 && diff.inHours == 0 || diff.inHours > 0 && diff.inDays == 0) {
         time = format.format(date);
      } else if (diff.inDays > 0 && diff.inDays < 7) {
         if (diff.inDays == 1) {
            time = diff.inDays.toString() + ' day ago';
         } else {
            time = diff.inDays.toString() + ' days ago';
         }
      } else {
         if (diff.inDays == 7) {
            time = (diff.inDays / 7).floor().toString() + ' week ago';
         } else {
            time = (diff.inDays / 7).floor().toString() + ' weeks ago';
         }
      }
      return time;
   }
}