

import 'package:intl/intl.dart';

class Group {
  int id;
  String name;
  int project_id;
  int type_steel_id;
  int total;
  String created_date;
  String created_by;
  int delete_flg;


  Group(this.id, this.name, this.project_id, this.type_steel_id, this.total,
      this.created_date, this.created_by, this.delete_flg);

  factory Group.fromJson(Map<String, dynamic> json) {
    String dateStr =  readTimestamp(json["created_date"]);
    return Group(
        json["id"] as int,
        json["name"] as String,
        json["project_id"] as int,
        json["type_steel_id"] as int,
        json["total"] as int,
        dateStr,
        json["created_by"] as String,
        0);
  }


   Map toObject(){
    Group group = this;
    return {
      "id": "${group.id}",
      "name": "${group.name}",
      "project_id": "${group.project_id}",
      "type_steel_id": "${group.type_steel_id}",
      "total": "${group.total}",
      "created_date": "${group.created_date}",
      "created_by": "${group.created_by}",
      "delete_flg": "${group.delete_flg}",
    };
  }

  static String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm a');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 || diff.inSeconds > 0 && diff.inMinutes == 0 || diff.inMinutes > 0 && diff.inHours == 0 || diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' DAY AGO';
      } else {
        time = diff.inDays.toString() + ' DAYS AGO';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
      } else {

        time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
      }
    }
    return time;
  }

}