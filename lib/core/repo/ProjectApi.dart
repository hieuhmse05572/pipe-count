import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class ProjectApi {
  Future<Response> getProjects(String name) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/projects?name=' + name;
      Response response = await get(url,
              headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );

      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }

  Future<Response> deleteProjectById(int projectId) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/projects/${projectId}';
      Response response = await delete(url,
          headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
  Future<Response> createProject(Project project) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/projects';
      Response response = await post(url,
          headers: NetworkUtil.getRequestHeaders(),
          body: json.encode(project.toObject()),
          encoding: Encoding.getByName("utf-8")
      )
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
}
