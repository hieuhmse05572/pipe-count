import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/image.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class ImageApi {
  Future<Response> getImagesByProjectId(int groupId, int projectId, int page, int sizePerPage) async {
    try {
      // int offset = sizePerPage * page + 1;
      int offset = 0;
      final url = NetworkUtil.BASE_URL + '/api/images?groupId=${groupId}&projectId=${projectId}&limit=${sizePerPage}&offset=${offset}' ;
      Response response = await get(url,
              headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }

  Future<Response> deleteImageById(int groupId) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/images/${groupId}';
      Response response = await delete(url,
          headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
  Future<Response> createImage(Image image) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/images';
      Response response = await post(url,
          headers: NetworkUtil.getRequestHeaders(),
          body: json.encode(image.toObject())
      )
          .timeout(
        Duration(seconds: 10),
      );
      print(response.body);
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }

  Future<Response> updateImage(Image image) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/images';
      Response response = await put(url,
          headers: NetworkUtil.getRequestHeaders(),
          body: json.encode(image.toObject())
      )
          .timeout(
        Duration(seconds: 10),
      );
      print(response.body);
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
}
