import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class LoginApi {
  Future<Response> login(String username, String password) async {
    username = username.trim();
    password = password.trim();
    try {
      final url = NetworkUtil.BASE_URL + '/api/login';
      Response response = await post(url,
              headers: NetworkUtil.getRequestHeadersWithoutAuth(),
              body: json.encode(NetworkUtil.getBody(username, password)))
          .timeout(
        Duration(seconds: 10),
      );
      print(response.body);
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
}
