import 'dart:async';
import 'dart:io';
import 'package:image/image.dart' ;
import 'package:http/http.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class UploadFileApi {

  Future<String> uploadImage(filename) async {
    try {
      // print(filename);
      // File image_File = File(filename);
      // Image image_temp = decodeImage(image_File.readAsBytesSync());
      // print("image_temp");
      // print(image_temp.height);
      // Image resized_img = copyResize(image_temp, height: 1080, width: 1920);
      // print("resized");
      // print(resized_img.height);
      // String fileName = "/storage/emulated/0/Android/data/com.example.pipe_count/files/Pictures/${new DateTime.now().millisecondsSinceEpoch}resized_img.png";
      //
      // File(fileName)..writeAsBytesSync(encodePng(resized_img));

    String url = NetworkUtil.BASE_URL + "/api/file/upload/images";
    var request = MultipartRequest('POST', Uri.parse(url));
    request.files.add(await MultipartFile.fromPath('file', filename));
    request.headers.addAll(NetworkUtil.getRequestHeaders());
      var res = await request.send().timeout(const Duration(seconds: 50));
      String str = await res.stream.bytesToString();
      return str;
      // Remaining code
    } on Exception catch (e) {
      print(e);
      return "timeout";
    }
  }
  Future<String> getPosition(filename) async {
    try {
      String url = NetworkUtil.BASE_URL + "/api/file/getPosition?fileName="  + filename;
      var request = MultipartRequest('POST', Uri.parse(url));
      request.headers.addAll(NetworkUtil.getRequestHeaders());
      var res = await request.send().timeout(const Duration(seconds: 50));
      String str = await res.stream.bytesToString();
      return str;
      // Remaining code
    } on Exception catch (e) {
      print(e);
      return "timeout";
    }
  }

  Future<String> saveImage(filename) async {
    try {
      String url = NetworkUtil.BASE_URL + "/api/file/upload";
      // String url = NetworkUtil.BASE_URL + "/api/file/upload/images";
      var request = MultipartRequest('POST', Uri.parse(url));
      request.files.add(await MultipartFile.fromPath('file', filename));
      request.headers.addAll(NetworkUtil.getRequestHeaders());
      var res = await request.send().timeout(const Duration(seconds: 50));
      String str = await res.stream.bytesToString();
      print(str);
      return str;
      // Remaining code
    } on Exception catch (e) {
      print(e);
      return "timeout";
    }
  }
}
