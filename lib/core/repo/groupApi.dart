import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class GroupApi {
  Future<Response> getGroupsByProjectId(String projectId) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/groups?projectId=${projectId}' ;
      Response response = await get(url,
              headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }

  Future<Response> deleteGroupById(int groupId) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/groups/${groupId}';
      Response response = await delete(url,
          headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
  Future<Response> createGroup(Group group) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/groups';
      Response response = await post(url,
          headers: NetworkUtil.getRequestHeaders(),
          body: json.encode(group.toObject())
      )
          .timeout(
        Duration(seconds: 10),
      );
      print(response.body);
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
}
