import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/models/type.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class TypeApi {
  Future<Response> getTypes(String name) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/types?name=${name}' ;
      Response response = await get(url,
              headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }

  Future<Response> deleteTypeById(int typeId) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/types/${typeId}';
      Response response = await delete(url,
          headers: NetworkUtil.getRequestHeaders())
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
  Future<Response> createType(TypeSteel type) async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/types';
      Response response = await post(url,
          headers: NetworkUtil.getRequestHeaders(),
          body: json.encode(type.toObject())
      )
          .timeout(
        Duration(seconds: 10),
      );
      return response;
    } catch (e) {
      return Response("error", 404);
    }
  }
}
