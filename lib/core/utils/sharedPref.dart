
import 'package:shared_preferences/shared_preferences.dart';

class SharePref {
  static SharedPreferences prefs;
  static SharePref _inst = SharePref._internal();
  SharePref._internal();

  static init() async {
    prefs = await SharedPreferences.getInstance();
  }

  factory SharePref() {
    if(_inst == null){
      _inst = SharePref._internal();
    }
    return _inst;
  }

  static Future<String> getString(String key) async {
    if(prefs == null){
      prefs = await SharedPreferences.getInstance();
    }
    return prefs.getString(key);
  }

  static updateSwitch(String text){
    bool value = prefs.getBool(text);
    if(value != null){
      return !value;
    }else{
       prefs.setBool(text, false);
       return false;
    }
  }
}
