import 'package:http/http.dart';

class NetworkUtil {
  String TOKEN = "";
  static final NetworkUtil _inst = NetworkUtil._internal();

  static String BASE_URL = "https://pipe.nsmv.com.vn";
  static Map<String, String> getRequestHeadersWithoutAuth() {
    return {
      "Content-Type": "application/json",
    };
  }

  static Map<String, String> getRequestHeaders() {
    return {
      'Content-Type': 'application/json; charset=UTF-8',
      "Authorization": "${_inst.TOKEN}",
    };
  }


  static Map getBody(String username, String password) {
    return {"userName": "$username", "password": "$password"};
  }

  static Map<String, String> getRequestHeaders2(int length) {
    return {
      'Content-Type': 'application/json; charset=UTF-8',
      "Authorization": "${_inst.TOKEN}",
      "Content-Length": "$length"
    };
  }

  NetworkUtil._internal();

  factory NetworkUtil(String token) {
    if (token != "") _inst.TOKEN = token;
    return _inst;
  }
  static Future<String> uploadImage(filename, url) async {
    print(_inst.TOKEN);
    var request = MultipartRequest('POST', Uri.parse(url));
    request.files.add(await MultipartFile.fromPath('picture', filename));
    request.headers.addAll(getRequestHeaders());
    var res = await request.send();
    return res.reasonPhrase;
  }
}
