
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class ImageUtil{



 static Future<Uint8List> readFileByte(String filePath) async {
    Uri myUri = Uri.parse(filePath);
    File f = new File.fromUri(myUri);
    Uint8List bytes;
    await f.readAsBytes().then((value) {
      bytes = Uint8List.fromList(value);

      print('reading of bytes is completed');
    }).catchError((onError) {
      print('Exception Error while reading audio from path:' +
          onError.toString());
    });
    return bytes;
  }

  static Future<ui.Image> loadImage(List<int> img) async {
    final Completer<ui.Image> completer = Completer();
    ui.decodeImageFromList(img, (ui.Image img) {
      return completer.complete(img);
    });
    return completer.future;
  }

  static Future<ui.Image> loadImageFromNet(String path) async {
   Completer<ImageInfo> completer = Completer();
   var img = new NetworkImage(path);
   img.resolve(ImageConfiguration()).addListener(ImageStreamListener((ImageInfo info,bool _){
     completer.complete(info);
   }));
   ImageInfo imageInfo = await completer.future;
   print(imageInfo.image);
   return imageInfo.image;
 }
}