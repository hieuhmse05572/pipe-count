import 'dart:convert';

import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/models/type.dart';
import 'package:pipe_count/core/repo/LoginApi.dart';
import 'package:pipe_count/core/repo/ProjectApi.dart';
import 'package:pipe_count/core/repo/groupApi.dart';
import 'package:pipe_count/core/repo/typeApi.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class TypeHelper {
  final api = TypeApi();
  Future<List<TypeSteel>> getTypes(String name) async {
    final apiResult = await api.getTypes(name);
    if (apiResult.statusCode == 200) {
      List<TypeSteel> types = new List();
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        for (Map i in data['body']) {
          TypeSteel t = TypeSteel.fromJson(i);
          types.add(t);
        }

        return types;
      }
    }
    return null;
  }

  Future<bool> deleteTypeById(int typeId) async {
    final apiResult = await api.deleteTypeById(typeId);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        print(data['body']);
        return data['body'];
      }
    }
    return false;
  }

  Future<bool> createType(TypeSteel type) async {
    final apiResult = await api.createType(type);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        print(data['body']);
        return data['body'];
      }
    }
    return false;
  }
}
