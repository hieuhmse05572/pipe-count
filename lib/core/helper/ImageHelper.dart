import 'dart:convert';

import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/image.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/repo/LoginApi.dart';
import 'package:pipe_count/core/repo/ProjectApi.dart';
import 'package:pipe_count/core/repo/groupApi.dart';
import 'package:pipe_count/core/repo/imageApi.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class ImageHelper {
  final api = ImageApi();
  Future<List<Image>> getImages(int groupId, int projectId, int page, int sizePerPage) async {
    final apiResult = await api.getImagesByProjectId(groupId, projectId, page, sizePerPage);
    if (apiResult.statusCode == 200) {
      List<Image> images = new List();
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        for (Map i in data['body']) {
          Image img= Image.fromJson(i);
          images.add(img);
        }
        return images;
      }
    }
    return null;
  }

  Future<bool> deleteImageById(int imageId) async {
    final apiResult = await api.deleteImageById(imageId);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      return true;
    }
    return false;
  }

  Future<bool> createImage(Image image) async {
    final apiResult = await api.createImage(image);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      // final data = jsonDecode(apiResult.body);
      // if (data['body'] != null) {
      //   print(data['body']);
      //   return data['body'];
      // }
      return true;
    }
    return false;
  }

  Future<bool> updateImage(Image image) async {
    final apiResult = await api.updateImage(image);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        print(data['body']);
        return data['body'];
      }
    }
    return false;
  }
}
