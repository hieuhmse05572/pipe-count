import 'dart:convert';

import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/repo/LoginApi.dart';
import 'package:pipe_count/core/repo/ProjectApi.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class ProjectHelper {
  final api = ProjectApi();
  Future<List<Project>> getProjects(String name) async {
    final apiResult = await api.getProjects(name);
    if (apiResult.statusCode == 200) {
      List<Project> projects = new List();
      final decodeData = utf8.decode(apiResult.bodyBytes);
      final data = jsonDecode(decodeData);
      // json.decode(utf8.decode(apiResult.body));
      if (data['body'] != null) {
        for (Map i in data['body']) {
          Project p = Project.fromJson(i);
          projects.add(p);
        }
        // data['body'].forEach((element) {
        //     print(element);
        //     // Project.fromJson(json);
        // });
        return projects;
      }
    }
    return null;
  }

  Future<bool> deleteProjectById(int projectId) async {
    final apiResult = await api.deleteProjectById(projectId);
    if (apiResult.statusCode == 200) {
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        return data['body'];
      }
    }
    return false;
  }

  Future<bool> createProject(Project project) async {
    final apiResult = await api.createProject(project);
    if (apiResult.statusCode == 200) {
      return true;
    }
    return false;
  }
}
