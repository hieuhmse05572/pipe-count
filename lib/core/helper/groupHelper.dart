import 'dart:convert';

import 'package:pipe_count/core/models/image.dart';
import 'package:pipe_count/core/helper/ImageHelper.dart';
import 'package:pipe_count/core/repo/imageApi.dart';
import 'package:pipe_count/core/models/group.dart';
import 'package:pipe_count/core/models/project.dart';
import 'package:pipe_count/core/repo/LoginApi.dart';
import 'package:pipe_count/core/repo/ProjectApi.dart';
import 'package:pipe_count/core/repo/groupApi.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class GroupHelper {
  final api = GroupApi();
  final imageHelper = ImageHelper();
  Future<List<Group>> getGroups(String projectId) async {
    final apiResult = await api.getGroupsByProjectId(projectId);
    if (apiResult.statusCode == 200) {
      List<Group> groups = new List();
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        for (Map i in data['body']) {
          Group group = Group.fromJson(i);
          // List<Image> image = await imageHelper.getImages(group.id, int.parse(projectId), 0, 1000);
          // int total = 0;
          // if (image != null) {
          //   image.forEach((e) {
          //     total += e.total;
          //   });
          //   group.total = total;
          // }
          groups.add(group);
        }
        // data['body'].forEach((element) {
        //     print(element);
        //     // Project.fromJson(json);
        // });
        // groups.forEach((element) {
        //   print(element.total);
        // });
        return groups;
      }
    }
    return null;
  }

  // List<Group> updateTotalGroup(List<Group> groups) {
  //   List<Group> newGroups = new List();
  //   groups.forEach((element) {
  //     imageHelper.getImages(element.id).then((value) {
  //       int total = 0;
  //       value.forEach((e) {
  //         total += e.total;
  //       });
  //       element.total = total;
  //     });
  //     newGroups.add(element);
  //   });
  //   return newGroups;
  // }

  Future<bool> deleteGroupById(int groupId) async {
    final apiResult = await api.deleteGroupById(groupId);
    if (apiResult.statusCode == 200) {
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        return data['body'];
      }
    }
    return false;
  }

  Future<bool> createGroup(Group group) async {
    final apiResult = await api.createGroup(group);
    if (apiResult.statusCode == 200) {
      return true;
        // final data = jsonDecode(apiResult.body);
        // if (data['body'] != null) {
        //   print(data['body']);
        //   return data['body'];
        // }
    }
    return false;
  }
}
