import 'dart:convert';

import 'package:pipe_count/core/repo/LoginApi.dart';
import 'package:pipe_count/core/utils/NetworkUtil.dart';

class LoginHelper {
  final api = LoginApi();
  Future<bool> login(String user, String password) async {
    final apiResult = await api.login(user, password);
    print(apiResult.statusCode);
    if (apiResult.statusCode == 200) {
      final data = jsonDecode(apiResult.body);
      if (data['body'] != null) {
        NetworkUtil(data['body']['accessToken']);
        print('auth');
        print(data);
        return true;
      }
    }
    return false;
  }
}
