import 'dart:async';
import 'dart:convert';

import 'package:pipe_count/core/models/circle.dart';
import 'package:pipe_count/core/repo/uploadFileApi.dart';


class UploadFileHelper {
  final api = UploadFileApi();

  Future<String> uploadFile(String imagePath) async {
    try {
      final apiResult = await api.uploadImage(imagePath);
      if (apiResult == "timeout") return null;
      return apiResult;
    } on Exception catch (e) {
      print("e");
      return null;
    }
    return null;
  }

  Future<List<Circle>> getPosition(String imagePath) async {
    try {
      final apiResult = await api.getPosition(imagePath);
      if (apiResult == "timeout") return null;
      final data = jsonDecode(apiResult);
      if (data != null) {
        List<Circle> circles = List();
        for (Map i in data) {
          Circle circle = Circle.fromJson(i);
          circles.add(circle);
        }
        return circles;
      }
    } on Exception catch (e) {
      print("e");
      return null;
    }
    return null;
  }

  Future<String> saveImage(String imagePath) async {
    try {
      final apiResult = await api.saveImage(imagePath);
      if (apiResult == "timeout") return null;
      if (apiResult != null) {
        return apiResult;
      }
    } on Exception catch (e) {
      print("e");
      return null;
    }
    return null;
  }

}
