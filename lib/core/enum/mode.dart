enum MODE{
  draw,
  edit
}

enum EDIT{
  move,
  drag,
  add,
  remove,
  none
}

enum DRAW{
  move,
  draw,
  clear,
  none
}

